mkdir -p $HOME/.local/share/systemd/user
sudo cp MidiKeys.service $HOME/.local/share/systemd/user
systemctl  --user enable MidiKeys.service
systemctl --user start MidiKeys.service
journalctl --user -xfu MidiKeys.service