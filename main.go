package main

import (
	"encoding/json"
	"flag"
	"github.com/micmonay/keybd_event"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/reader"
	"gitlab.com/gomidi/portmididrv"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"time"
)

//region Structs

type KeySetting struct {
	Alt    bool
	AltGR  bool
	Ctrl   bool
	CtrlR  bool
	Shift  bool
	ShiftR bool
	Super  bool
	Keys   int
}
type Flags struct {
	createSettings *bool
	filePath       *string
	deviceId       *int
	printMidiIns   *bool
	PrintMidiMsg   *bool
}

//endregion

var logger = log.New(os.Stdout, "Output", log.LstdFlags)
var debug = log.New(os.Stdout, "Debug", log.LstdFlags)
var flags Flags

func main() {
	flags = ParsFlags()

	if *flags.createSettings {
		marshal := CreateExampleJson()
		logger.Println("Creating Example Config file in: ", *flags.filePath)
		ioutil.WriteFile(*flags.filePath, marshal, 0644)
		os.Exit(1)
	}

	ins := SetupMIDI()

	if *flags.printMidiIns {
		printInPorts(ins)
		os.Exit(1)
	}

	in := SetupMidiIn(ins)

	settings := SetupJsonSettings()

	kb := SetupKeyBonding()

	setupListener(in, settings, kb)

	os.Exit(0)
}

//region Midi

func setupListener(in midi.In, settings map[byte]*KeySetting, kb keybd_event.KeyBonding) error {
	err := in.SetListener(func(data []byte, deltaMicroseconds int64) {
		if *flags.PrintMidiMsg {
			logger.Println(data)
		}

		midikey := data[1]

		setting := settings[midikey]
		if setting != nil && data[0] == 128 {
			SetKeyBondingKeys(&kb, setting)
			if err := kb.Launching(); err != nil {
				logger.Println("Error Pressing virtual Key: ", err)
			}
		}
	})
	if err != nil {
		logger.Println("Error while Setting up Listener, ", err)
		return err
	}
	// listen for MIDI
	rd := reader.New()
	return rd.ListenTo(in)
}

func SetupMidiIn(ins []midi.In) midi.In {
	in := ins[*flags.deviceId]
	if in == nil {
		logger.Println("This device does not exist")
		os.Exit(1)
	}

	err := in.Open()
	if err != nil {
		logger.Println("Could not open Midi input: ", err)
		panic(err)
	}
	return in
}

func SetupMIDI() []midi.In {
	drv, err := portmididrv.New()
	if err != nil {
		logger.Println("Problems while reading Midi, is PortMidi Installed?: ", err)
		panic(err)
	}

	// make sure to close all open ports at the end
	defer drv.Close()

	ins, err := drv.Ins()
	if err != nil {
		logger.Println("No Input Midi devices: ", err)
		panic(err)
	}
	return ins
}

func printPort(port midi.Port) {
	logger.Printf("PortNumber: [%v]%s\n", port.Number(), port.String())
}

func printInPorts(ports []midi.In) {
	logger.Printf("MIDI IN Ports\n")
	for _, port := range ports {
		printPort(port)
	}
}

//endregion

//region Runtime

func SetupJsonSettings() map[byte]*KeySetting {
	file, err := ioutil.ReadFile(*flags.filePath)
	if err != nil {
		logger.Println("File Error: ", err)
		panic(err)
	}

	settings := map[byte]*KeySetting{}
	err = json.Unmarshal(file, &settings)
	if err != nil {
		logger.Println("Setting file has Error: ", err)
		panic(err)
	}
	return settings
}

func CreateExampleJson() []byte {
	example := map[byte]*KeySetting{
		byte(50): {
			Alt:    true,
			AltGR:  false,
			Ctrl:   false,
			CtrlR:  false,
			Shift:  false,
			ShiftR: false,
			Super:  false,
			Keys:   193,
		}, byte(51): {
			Alt:    true,
			AltGR:  false,
			Ctrl:   false,
			CtrlR:  false,
			Shift:  false,
			ShiftR: false,
			Super:  false,
			Keys:   194,
		}}
	marshal, err := json.Marshal(example)
	if err != nil {
		logger.Println("Error while Marshaling Test Json", err)
		os.Exit(2)
	}
	return marshal
}

func ParsFlags() Flags {
	flags := Flags{
		createSettings: flag.Bool("CreateSetting", false, ""),
		filePath:       flag.String("JsonPath", "KeySettings.json", "/path/to/file.json"),
		printMidiIns:   flag.Bool("PrintIns", false, ""),
		deviceId:       flag.Int("DeviceID", 1, ""),
		PrintMidiMsg:   flag.Bool("PrintMSG", false, ""),
	}

	flag.Parse()
	logger.Println("Programm Flags are:", flags)
	return flags
}

//endregion

//region Key Mapping

func SetKeyBondingKeys(kb *keybd_event.KeyBonding, setting *KeySetting) {
	debug.Println("Setting Bonding to: ", setting)
	kb.SetKeys(setting.Keys)
	kb.HasALT(setting.Alt)
	kb.HasALTGR(setting.AltGR)
	kb.HasCTRL(setting.Ctrl)
	kb.HasCTRLR(setting.CtrlR)
	kb.HasSHIFT(setting.Shift)
	kb.HasSHIFTR(setting.ShiftR)
	kb.HasSuper(setting.Super)
}

func SetupKeyBonding() keybd_event.KeyBonding {
	kb, err := keybd_event.NewKeyBonding()
	if err != nil {
		logger.Println("Error while Key Bonding: ", err)
		panic(err)
	}

	// For linux, it is very important to wait 2 seconds
	if runtime.GOOS == "linux" {
		time.Sleep(2 * time.Second)
	}
	return kb
}

//endregion
