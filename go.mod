module miditomacro

go 1.14

require (
	github.com/micmonay/keybd_event v1.1.0
	gitlab.com/gomidi/midi v1.16.5
	gitlab.com/gomidi/portmididrv v0.6.0
)
